package com.gopaisa.adapter;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gopaisa.R;
import com.gopaisa.model.Entry;
import com.gopaisa.util.GlideUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Atiq on 02/04/17.
 */

public class EntryAdapter extends RecyclerView.Adapter<EntryAdapter.ViewHolder> {


    private List<Entry> list=new ArrayList<>();
    private Activity context;
    private int resource;



    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_entry) ImageView entry_image;
        @BindView(R.id.txt_name) TextView entry_name;
        @BindView(R.id.txt_price) TextView entry_price;
        @BindView(R.id.txt_summary) TextView entry_summary;



        public ViewHolder(@Nullable View view){
            super(view);
            ButterKnife.bind(this, view);
            entry_image=(ImageView)view.findViewById(R.id.iv_entry);
            entry_name=(TextView) view.findViewById(R.id.txt_name);

            entry_price=(TextView) view.findViewById(R.id.txt_price);
            entry_summary=(TextView) view.findViewById(R.id.txt_summary);
        }

       /* public void bind(final LearningPlansVedio learningPlansVedio, final int position,final String title) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                }
            });
        }*/
    }


    public EntryAdapter(Activity context, int resource, List<Entry> list) {
        this.resource = resource;
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_row, parent, false);


        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Entry entry = list.get(position);

        final String thumbnailUrl =entry.getImImage().get(0).getLabel() ;

        GlideUtil.loadImageWithDefaultImage(context, holder.entry_image, thumbnailUrl, R.mipmap.ic_launcher);


        holder.entry_name.setText(entry.getImName().getLabel());

        holder.entry_price.setText(entry.getImPrice().getLabel());
        holder.entry_summary.setText(entry.getSummary().getLabel());

        //     holder.video_title.setText(learningPlansVedio.getName());
      //  holder.bind(learningPlansVedio,position,title);



    }

    public Entry getItem(int pos){
        return list.get(pos);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
