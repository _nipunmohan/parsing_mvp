/*
 *
 *  * Copyright (C) 2014 Antonio Leiva Gordillo.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.gopaisa.presentor;

import com.gopaisa.model.Entry;
import com.gopaisa.model.Gopaisa;

import java.util.ArrayList;
import java.util.List;

public class GoPaisaPresenterImpl implements GoPaisaPresenter, GoPaisaInteractor.OnDataDownloadFinishListener {

    private GoPaisaView goPaisaView;
    private GoPaisaInteractor goPaisaInteractor;

    public GoPaisaPresenterImpl(GoPaisaView goPaisaView) {
        this.goPaisaView = goPaisaView;
        this.goPaisaInteractor = new GoPaisaInteractorImpl();
    }


    @Override
    public void downloadData() {
        goPaisaInteractor.downloadData(this);
    }

    @Override public void onDestroy() {
        goPaisaView = null;
    }



    @Override
    public void onDownloadSuccess(List<Entry> list) {
        if (goPaisaView != null) {
            goPaisaView.displayData(list);
        }
    }
}
