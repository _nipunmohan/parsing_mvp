package com.gopaisa.presentor;

import com.gopaisa.GoPaisaApplication;
import com.gopaisa.database.GoPaisaDao;
import com.gopaisa.executor.BackgroundExecutor;
import com.gopaisa.executor.GoPaisaRequesterJson;
import com.gopaisa.executor.GoPaisaRequesterXml;
import com.gopaisa.utils.NetworkUtil;

public class GoPaisaInteractorImpl implements GoPaisaInteractor {

    @Override
    public void downloadData( final OnDataDownloadFinishListener listener) {
        if (NetworkUtil.isNetworkAvailable()){
           // BackgroundExecutor.getInstance().execute(new GoPaisaRequesterXml("xml", GoPaisaApplication.getInstance(),listener));
            BackgroundExecutor.getInstance().execute(new GoPaisaRequesterJson("json", GoPaisaApplication.getInstance(),listener));
        }else{
            listener.onDownloadSuccess(GoPaisaDao.getInstance(GoPaisaApplication.getInstance()).getAllEntryList());
        }
    }
}
