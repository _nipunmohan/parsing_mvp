package com.gopaisa.executor;

/**
 * Created by Atiq on 14/02/17.
 */

public interface AbstractRequester {
    void execute();
}
