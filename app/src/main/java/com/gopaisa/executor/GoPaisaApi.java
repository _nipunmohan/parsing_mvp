package com.gopaisa.executor;

import com.gopaisa.model.Feed;
import com.gopaisa.model.Gopaisa;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Atiq on 21/9/16.
 */

public interface GoPaisaApi {
    @GET("WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/sf=143441/limit=100/genre=6007/{data_formate}")
    Observable<Gopaisa> getEntryListJson(@Path("data_formate") String dataFormate);

    @Headers({
            "Content-Type: text/xml",
            "Accept-Charset: utf-8"
    })
    @GET("WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/sf=143441/limit=100/genre=6007/{data_formate}")
    Observable<Feed> getEntryListXml(@Path("data_formate") String dataFormate);
}
