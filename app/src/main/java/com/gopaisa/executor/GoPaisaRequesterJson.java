package com.gopaisa.executor;

import android.content.Context;
import android.util.Log;

import com.gopaisa.GoPaisaApplication;
import com.gopaisa.database.GoPaisaDao;
import com.gopaisa.model.Gopaisa;
import com.gopaisa.presentor.GoPaisaInteractor;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Atiq on 14/02/17.
 */

public class GoPaisaRequesterJson implements AbstractRequester{
    private String formator;
    private Context context;
    private GoPaisaInteractor.OnDataDownloadFinishListener onDataDownloadFinishListener;

    public GoPaisaRequesterJson(String formator, Context context, GoPaisaInteractor.OnDataDownloadFinishListener onDataDownloadFinishListener) {
        this.formator = formator;
        this.context = context;
        this.onDataDownloadFinishListener = onDataDownloadFinishListener;
    }



    @Override
    public void execute() {
        final Observable<Gopaisa> gopaisaObservable = GoPaisaApplication.getInstance().getGoPaisaApiJson().getEntryListJson(formator);


        gopaisaObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Gopaisa>() {
                    @Override
                    public final void onCompleted() {
                        // do nothing
                    }
                    @Override
                    public final void onError(Throwable e) {
                        Log.e("Throwable", e.getMessage());
                     //   EventBus.getDefault().post(new EventObject(EventCenter.CLASS_VIDEO_ERROR, e));
                    }
                    @Override
                    public final void onNext(Gopaisa gopaisa) {
                        GoPaisaDao.getInstance(context).addDataInDB(gopaisa.getFeed().getEntry());
                        onDataDownloadFinishListener.onDownloadSuccess(gopaisa.getFeed().getEntry());
                    }
                });

    }
}
