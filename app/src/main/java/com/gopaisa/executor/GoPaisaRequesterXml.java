package com.gopaisa.executor;

import android.content.Context;
import android.util.Log;

import com.gopaisa.GoPaisaApplication;
import com.gopaisa.database.GoPaisaDao;
import com.gopaisa.model.Feed;
import com.gopaisa.model.Gopaisa;
import com.gopaisa.presentor.GoPaisaInteractor;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Atiq on 14/02/17.
 */

public class GoPaisaRequesterXml implements AbstractRequester{
    private String formator;
    private Context context;
    private GoPaisaInteractor.OnDataDownloadFinishListener onDataDownloadFinishListener;

    public GoPaisaRequesterXml(String formator, Context context, GoPaisaInteractor.OnDataDownloadFinishListener onDataDownloadFinishListener) {
        this.formator = formator;
        this.context = context;
        this.onDataDownloadFinishListener = onDataDownloadFinishListener;
    }



    @Override
    public void execute() {

        final Observable<Feed> gopaisaObservable = GoPaisaApplication.getInstance().getGoPaisaApiXml().getEntryListXml(formator);
        gopaisaObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Feed>() {
                    @Override
                    public final void onCompleted() {
                        // do nothing
                    }
                    @Override
                    public final void onError(Throwable e) {
                        Log.e("Throwable", e.getMessage());
                    }
                    @Override
                    public final void onNext(Feed feed) {
                        GoPaisaDao.getInstance(context).addDataInDB(feed.getEntry());
                        onDataDownloadFinishListener.onDownloadSuccess(feed.getEntry());
                    }
                });

    }
}
