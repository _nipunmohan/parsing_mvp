package com.gopaisa.executor;

public class BackgroundExecutor {
    private static BackgroundExecutor _bgExecutor;


    private BackgroundExecutor() {
    }

    public static BackgroundExecutor getInstance() {
        if (_bgExecutor == null) {
            _bgExecutor = new BackgroundExecutor();
        }

        return _bgExecutor;
    }

    public void execute(AbstractRequester requester) {
        if(requester!=null)
        requester.execute();
    }

}
