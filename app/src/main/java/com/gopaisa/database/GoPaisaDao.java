package com.gopaisa.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.gopaisa.enums.Tables;
import com.gopaisa.model.Entry;
import com.gopaisa.model.Gopaisa;
import com.gopaisa.model.ImImage;
import com.gopaisa.model.ImName;
import com.gopaisa.model.ImPrice;
import com.gopaisa.model.Summary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Atiq on 4/2/2017.
 */
public class GoPaisaDao {
    public static final String TAG = GoPaisaDao.class.getName();

    private SQLiteDatabase database;
    private SQLiteHelper dbHelper = null;
    private static GoPaisaDao _instance;
    private Context _context;

    private GoPaisaDao(Context context) {
        if (dbHelper == null) {
            _context = context;
            dbHelper = SQLiteHelper.getInstance(context);
        }
    }

    public static GoPaisaDao getInstance(Context context) {
        if (_instance == null) {
            _instance = new GoPaisaDao(context);
        }
        return _instance;
    }

    public boolean isOpen() {
        if (database != null) {
            return database.isOpen();
        }
        return false;
    }

    public void open() throws SQLException {
        if (dbHelper == null) {
            dbHelper = SQLiteHelper.getInstance(_context);
        } else {
            database = dbHelper.getWritableDatabase();
        }
    }

    public void close() {
        if (dbHelper != null) {
            dbHelper.close();
        }
    }

   /* public void insertCountryDataInDBFromXML() {
        ArrayList<Location> locations = getCountryFromXML();
        addCountriesInDB(locations);
    }

    private ArrayList<Location> getCountryFromXML() {
        ArrayList<Location> locations = new ArrayList<Location>();
        try {
            XmlResourceParser parser = _context.getResources().getXml(R.xml.countrycode);
            parser.next();
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equals(ConstantUtil.COUNTRY)) {
                            String countryCode = parser.getAttributeValue(0);
                            String phoneCode = parser.getAttributeValue(1);
                            String name = parser.getAttributeValue(2);
                            if (countryCode != null && phoneCode != null && name != null) {
                                locations.add(new Location(phoneCode, name, countryCode, ""));
                            }
                        }
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }
            parser.close();
        } catch (Exception e) {
            Log.e(TAG, "Unable toread country from  xml ", e);
        }
        return locations;
    }*/

    public void addDataInDB(List<Entry> entryList) {
        if (!isOpen()) {
            open();
        }
        try {
            if (database != null) {
                String sql = "INSERT OR REPLACE INTO " + Tables.GOPAISA.getLabel() + " VALUES (?,?,?,?,?);";
                SQLiteStatement statement = database.compileStatement(sql);
                database.beginTransaction();
                for (Entry entry : entryList) {
                    statement.clearBindings();
                    statement.bindString(1, entry.getId().getLabel());
                    statement.bindString(2, entry.getImImage().get(0).getLabel());
                    statement.bindString(3, entry.getImPrice().getLabel());
                    statement.bindString(4, entry.getImName().getLabel());
                    statement.bindString(5, entry.getSummary().getLabel());
                    statement.execute();
                }
                database.setTransactionSuccessful();
                database.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Enable to Add Entry :", e);
        }
		/*
		 * if (isOpen()) { close(); }
		 */
    }



    public ArrayList<Entry> getAllEntryList() {
        if (!isOpen()) {
            open();
        }

        ArrayList<Entry> list = new ArrayList<Entry>();
        try {
            if (database != null) {
                Cursor cursor = database.query(Tables.GOPAISA.getLabel(), null, null, null, null, null, null);
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        Entry entry = new Entry();
                        ImName imName=new ImName();
                        imName.setLabel(cursor.getString(cursor.getColumnIndex(Tables.GoPaisa.COL_NAME.getLabel())));
                        entry.setImName(imName);

                        ImImage imImage=new ImImage();
                        imImage.setLabel(cursor.getString(cursor.getColumnIndex(Tables.GoPaisa.COL_IMG_URL.getLabel())));
                        List<ImImage> imImages=new ArrayList<>();
                        imImages.add(imImage);
                        entry.setImImage(imImages);

                        ImPrice imPrice=new ImPrice();
                        imPrice.setLabel(cursor.getString(cursor.getColumnIndex(Tables.GoPaisa.COL_PRICE.getLabel())));
                        entry.setImPrice(imPrice);

                        Summary summary=new Summary();
                        summary.setLabel(cursor.getString(cursor.getColumnIndex(Tables.GoPaisa.COL_SUMMARY.getLabel())));
                        entry.setSummary(summary);

                        list.add(entry);
                    }
                    cursor.close();
                }
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "Unable to get data from database", e);
        } catch (SQLException e) {
            Log.e(TAG, "Unable to get data from database", e);
        } catch (Exception e) {
            Log.e(TAG, "Unable to get data from database", e);
        }
        if (isOpen()) {
            close();
        }
        return list;
    }



}
