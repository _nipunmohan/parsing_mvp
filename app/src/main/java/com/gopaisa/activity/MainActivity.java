package com.gopaisa.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gopaisa.R;
import com.gopaisa.adapter.EntryAdapter;
import com.gopaisa.model.Entry;
import com.gopaisa.model.Gopaisa;
import com.gopaisa.presentor.GoPaisaPresenter;
import com.gopaisa.presentor.GoPaisaPresenterImpl;
import com.gopaisa.presentor.GoPaisaView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements GoPaisaView {
    private GoPaisaPresenter goPaisaPresenter;
    private RecyclerView entryRecyclerView;
    private ProgressBar pb;

    //http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/sf=143441/limit=100/genre=6007/json
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        entryRecyclerView=(RecyclerView)findViewById(R.id.rv_entrylist);
        pb=(ProgressBar) findViewById(R.id.pb);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setSmoothScrollbarEnabled(false);
        entryRecyclerView.setLayoutManager(layoutManager);

        goPaisaPresenter = new GoPaisaPresenterImpl(this);
        pb.setVisibility(View.VISIBLE);
        goPaisaPresenter.downloadData();

    }



    @Override
    public void displayData(List<Entry> entryList) {
        pb.setVisibility(View.GONE);
        if (entryList!=null || entryList.size()!=0){
            EntryAdapter entryAdapter=new EntryAdapter(this,R.layout.entry_row,entryList);
            entryRecyclerView.setAdapter(entryAdapter);
        }else {
            Toast.makeText(this,"No Data Avialable",Toast.LENGTH_SHORT).show();
        }

    }
}
