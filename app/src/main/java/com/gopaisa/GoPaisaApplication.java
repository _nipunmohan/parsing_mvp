package com.gopaisa;


import android.app.Application;

import com.gopaisa.executor.GoPaisaApi;

import java.io.IOException;
import java.util.UUID;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;


/**
 * Created by Atiq on 5/9/16.
 */

public class GoPaisaApplication extends Application {

    public static final String TAG = GoPaisaApplication.class.getName();

    private static GoPaisaApplication mInstance;


    private GoPaisaApi goPaisaApiJson;
    private GoPaisaApi goPaisaApiXml;
    private OkHttpClient client;
    private RxJavaCallAdapterFactory rxAdapter;
    protected String userAgent;

    public GoPaisaApplication() {
        mInstance = this;
    }

    public static GoPaisaApplication getInstance() {
        if(mInstance == null) {
            mInstance = new GoPaisaApplication();
        }
        return mInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        rxAdapter = RxJavaCallAdapterFactory.create();
        createClient();
        initRetrofitJson();
        initRetrofitXml();

    }




    public boolean useExtensionRenderers() {
        return BuildConfig.FLAVOR.equals("withExtensions");
    }



    private void initRetrofitJson(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .client(client)
                .build();
        goPaisaApiJson = retrofit.create(GoPaisaApi.class);
    }


    private void initRetrofitXml(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .client(client)
                .build();
        goPaisaApiXml = retrofit.create(GoPaisaApi.class);
    }

    public void createClient(){
        // Define the interceptor, add authentication headers
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder().addHeader("User-Agent", getUserAgent()).build();
                return chain.proceed(newRequest);
            }
        };

        // Add the interceptor to OkHttpClient
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        this.client = builder.build();
    }


        public GoPaisaApi getGoPaisaApiJson(){
            return goPaisaApiJson;
        }

    public GoPaisaApi getGoPaisaApiXml(){
        return goPaisaApiXml;
    }

    /**
     * This method return user agent.
     * @return
     */
    public String getUserAgent() {
        return "Mozilla/5.0 (X11; mandroid) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91  Safari/537.36" +
                getUniqueId();
    }



    public synchronized  String getUniqueId() {
        String  uniqueID ="";
        if (uniqueID == null) {
            uniqueID = UUID.randomUUID().toString();
        }
        return uniqueID;
    }

}
