
package com.gopaisa.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
@Root(name= "Id",strict = false)
public class Id {

    @Element(name = "label")
    @SerializedName("label")
    @Expose
    private String label;


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


}
