
package com.gopaisa.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name = "feed",strict = false)
public class Feed {

    @ElementList(name = "entry", inline=true)
    @SerializedName("entry")
    @Expose
    private List<Entry> entry = new ArrayList<>();

    public List<Entry> getEntry() {
        return entry;
    }

    public void setEntry(List<Entry> entry) {
        this.entry = entry;
    }



}
