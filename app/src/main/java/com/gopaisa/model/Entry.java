
package com.gopaisa.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(strict = false)
public class Entry {

    @Element(name = "im:name")
    @SerializedName("im:name")
    @Expose
    private ImName imName;

    @ElementList(name = "im:image", inline=true)
    @SerializedName("im:image")
    @Expose
    private List<ImImage> imImage = new ArrayList<>();

    @Element(name = "summary")
    @SerializedName("summary")
    @Expose
    private Summary summary;

    @Element(name = "im:price")
    @SerializedName("im:price")
    @Expose
    private ImPrice imPrice;


    @Element(name = "id")
    @SerializedName("id")
    @Expose
    private Id id;


    public ImName getImName() {
        return imName;
    }

    public void setImName(ImName imName) {
        this.imName = imName;
    }

    public List<ImImage> getImImage() {
        return imImage;
    }

    public void setImImage(List<ImImage> imImage) {
        this.imImage = imImage;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public ImPrice getImPrice() {
        return imPrice;
    }

    public void setImPrice(ImPrice imPrice) {
        this.imPrice = imPrice;
    }



    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }



}
