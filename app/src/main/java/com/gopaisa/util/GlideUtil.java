package com.gopaisa.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

/**
 * Created by Atiq on 24/9/16.
 */

public class GlideUtil {


    /**
     *
     * @param context
     * @param imageView
     * @param url
     */
    public static void loadImageCirculer(final Context context , final ImageView imageView , String url){
        Glide.with(context).load(url).asBitmap().centerCrop().fitCenter().into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }
    /***
     *
     * load image with default image
     *
     */

    public static void loadImageWithDefaultImage(final Context context , final ImageView imageView , String url,int defaultImage){
        Glide.with(context)
                .load(url)
                .asBitmap()
                .centerCrop()
                .error(defaultImage)
                .fitCenter()
                .placeholder(defaultImage)
                .into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }


    public static void loadImageWithDefaultImageFromDrawable(final Context context , final ImageView imageView , Drawable drawable, int defaultImage){
        Glide.with(context)
                .load("")
                .asBitmap()
                .placeholder(drawable)
                .centerCrop()
                .error(defaultImage)
                .fitCenter()
                .placeholder(defaultImage)
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }


    /**
     *
     * @param context
     * @param imageView
     * @param url
     */
    public static void loadImage(final Context context , final ImageView imageView , String url){
        Glide.with(context).load(url).asBitmap().centerCrop().fitCenter().into(imageView);
    }

}
