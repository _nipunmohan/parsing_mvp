package com.gopaisa.enums;

/**
 * Created by Atiq on 4/2/2017.
 */
public enum Tables {
    GOPAISA("gopaisa");


    private String label;

    private Tables(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }


    public enum GoPaisa {

        COL_ID("id"), COL_IMG_URL("imgurl"), COL_PRICE("price"), COL_NAME("name"), COL_SUMMARY("summary");
        private String label;

        private GoPaisa(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        public static String createTableQuery() {
            return "CREATE TABLE IF NOT EXISTS " + GOPAISA + "("
                    + COL_ID.getLabel() + " text PRIMARY KEY, " + COL_IMG_URL.getLabel()
                    + " text, "+COL_PRICE.getLabel()+" text, "+COL_NAME.getLabel()+" text, " + COL_SUMMARY.getLabel() + " text" + ")";
        }

    }


}
